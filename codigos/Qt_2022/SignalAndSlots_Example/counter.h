#ifndef COUNTER_H
#define COUNTER_H

#include <QObject>

class Counter : public QObject
{
    Q_OBJECT
public:
    explicit Counter(QObject *parent = nullptr);
    void setValue(int value);
public slots:
    void myPrint();
signals:
    void valueChanged();
private:
    int m_value;
};

#endif // COUNTER_H
