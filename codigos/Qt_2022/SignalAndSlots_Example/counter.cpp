#include "counter.h"
#include <QDebug>

Counter::Counter(QObject *parent) : QObject(parent)
{
    m_value=0;
}
void Counter::setValue(int value){
    if (m_value != value) {
        m_value = value;
        emit valueChanged();
    }
}
void Counter::myPrint(){
    qDebug() <<"Changed!" << m_value;
}
